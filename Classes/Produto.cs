﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_Stock.Classes
{
    public class Produto
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage ="Campo Obrigatorio")]
        public string nomeProduto { get; set; }

        [Required(ErrorMessage = "Campo Obrigatorio")]
        public int codigoBarra { get; set; }

        [Required(ErrorMessage = "Campo Obrigatorio")]
        public string marca { get; set; }

        [Required(ErrorMessage = "Campo Obrigatorio")]
        public float valorEntrada { get; set; }

        [Required(ErrorMessage = "Campo Obrigatorio")]
        public float valorVenda { get; set; }

        [Required(ErrorMessage = "Campo Obrigatorio")]
        public float estoque { get; set; }

    }
}