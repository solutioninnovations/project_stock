﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project_Stock.Classes
{
    public class Usuario
    {
        public string nome { get; set; }
        public string email{ get; set; }
        public string senha { get; set;}
        public string empresa { get; set; }
    }
}